import GlobalApp from 'GlobalApp'
// const path = "/static";
const path = "/static";
export default {
  version: 0,
  config: GlobalApp.config,
  gameSport: [
    {
      vendor: "SUPER",
      source: "SUPER",
      id: "SUPER",
      name: "superSport",
      nameZh: "Super 体育",
      // img: path + "/images/game/sport-01.png",
      img: path + "/images/game-vendor/super.png",
      logo: path + "/images/vendor-logo/super.png"

    },
    {
      vendor: "SXB",
      source: "SXB",
      id: "SXB",
      name: "SXBSport",
      nameZh: "赢家体育",
      // img: path + "/images/game/sport-02.png",
      img: path + "/images/game-vendor/winner.png",
      logo: path + "/images/vendor-logo/winner.png"
    }
    // {
    //   vendor: "CS",
    //   source: "CS",
    //   id: "CS",
    //   name: "CSSport"
    // }
  ],
  gameRealbet: [
    {
      vendor: "DG",
      source: "DG",
      id: "DG",
      name: "dgLive",
      nameZh: "DG 真人",
      // img: path + "/images/game/live-01.png",
      img: path + "/images/game-vendor/dg.png",
      logo: path + "/images/vendor-logo/dg.png"

    },

    {
      vendor: "SA_LIVE",
      source: "SA_LIVE",
      id: "SA_LIVE",
      name: "saLive",
      nameZh: "沙龙真人",
      // img: path + "/images/game/live-02.png",
      img: path + "/images/game-vendor/sa.png",
      logo: path + "/images/vendor-logo/sa.png"
    },

    {
      vendor: "ALLBET_LIVE",
      source: "ALLBET_LIVE",
      id: "ALLBET_LIVE",
      name: "allbetLive",
      nameZh: "欧博真人",
      // img: path + "/images/game/live-03.png",
      img: path + "/images/game-vendor/allbet.png",
      logo: path + "/images/vendor-logo/allbet.png"
    },

    {
      vendor: "WM",
      source: "WM",
      id: "WM",
      name: "wm",
      nameZh: "完美真人",
      // img: path + "/images/game/live-04.png",
      img: path + "/images/game-vendor/wm.png",
      logo: path + "/images/vendor-logo/wm.png"
    }
  ],
  gameChess: [
    // {
    //   vendor: "SHYQ",
    //   source: "SHYQ",
    //   id: "SHYQ",
    //   name: "SHYQ",
    //   nameZh: "幸運棋牌",
    //   // img: path + "/images/game/chess-02.png",
    //   img: path + "/images/game-vendor/lucky.png",
    //   logo: path + "/images/vendor-logo/lucky.png"
    // },
    // {
    //   vendor: "SY",
    //   source: "SY",
    //   id: "SY",
    //   name: "SY",
    //   nameZh: "雙贏娛樂城",
    //   // img: path + "/images/game/chess-04.png",
    //   img: path + "/images/game-vendor/sy.png",
    //   logo: path + "/images/vendor-logo/sy.png"
    // }
    // {
    //   vendor: "DT",
    //   source: "DT",
    //   id: "DT",
    //   name: "DT2",
    //   to: "/slot/dt2",
    //   img: path + "/images/game/chess-06.png"
    // }
    {
      vendor: "BOLE",
      source: "BOLE",
      id: "BOLE",
      name: "BOLE",
      nameZh: "博樂棋牌",
      // img: path + "/images/game/chess-04.png",
      img: path + "/images/game-vendor/sy.png",
      logo: path + "/images/vendor-logo/sy.png"
    }
  ],
  gameGaming: [
    // {
    //   vendor: "TFG",
    //   source: "TFG",
    //   id: "TFG",
    //   name: "TFG",
    //   nameZh: "雷火電競",
    //   // img: path + "/images/game/egame-01.png",
    //   img: path + "/images/game-vendor/tfg.png",
    //   logo: path + "/images/vendor-logo/tf.png"
    // }
  ],
  gameLottery: [
    {
      vendor: "CROWN",
      source: "CROWN",
      id: "CROWN",
      name: "lottery2",
      nameZh: "天心彩票",
      // img: path + "/images/game/lottery-03.png",
      img: path + "/images/game-vendor/zeus-lottery.png",
      logo: path + "/images/vendor-logo/zeus.png"
    },
    {
      vendor: "VIX",
      source: "VIX",
      id: "VIX",
      name: "VIX",
      nameZh: "VIX指数",
      // img: path + "/images/game/lottery-02.png",
      img: path + "/images/game-vendor/self-binary-cn.png",
      logo: path + "/images/vendor-logo/binary.png"
    }
  ],
  gameEgame: [
    // {
    //   vendor: "AE",
    //   source: "AE",
    //   id: "AE",
    //   name: "aeE",
    //   nameZh: "Ameba 電子",
    //   to: "/slot/ae",
    //   // img: path + "/images/game/slot-01.png",
    //   img: path + "/images/game-vendor/ameba.png",
    //   logo: path + "/images/vendor-logo/ameba.png"
    // },

    // {
    //   vendor: "IG",
    //   source: "IG",
    //   id: "IG",
    //   name: "igE",
    //   nameZh: "必贏電子",
    //   to: "/slot/ig",
    //   // img: path + "/images/game/slot-05.png",
    //   img: path + "/images/game-vendor/ig.png",
    //   logo: path + "/images/vendor-logo/ig.png"
    // },

    {
      vendor: "BNG",
      source: "BNG",
      id: "BNG",
      name: "BNG",
      nameZh: "BNG 电子",
      to: "/slot/bng",
      // img: path + "/images/game/slot-03.png",
      img: path + "/images/game-vendor/bng.png",
      logo: path + "/images/vendor-logo/bng.png"
    },

    {
      vendor: "DT",
      source: "DT",
      id: "DT",
      name: "DT",
      to: "/slot/dt",
      nameZh: "DT 老虎机",
      // img: path + "/images/game/slot-04.png",
      img: path + "/images/game-vendor/dt.png",
      logo: path + "/images/vendor-logo/dt.png"
    },

    {
      vendor: "ING",
      source: "ING",
      id: "ING",
      name: "ING",
      nameZh: "ING 电子",
      // img: path + "/images/game/slot-02.png",
      img: path + "/images/game-vendor/ing.png",
      logo: path + "/images/vendor-logo/ing.png"
    },
    // {
    //   vendor: "JDB",
    //   source: "MS",
    //   id: "MS",
    //   name: "JDB",
    //   nameZh: "JDB 捕鱼",
    //   // img: path + "/images/game/slot-02.png",
    //   img: path + "/images/game-vendor/jdb.png",
    //   logo: path + "/images/vendor-logo/jdb.png"
    // },
    {
      vendor: "MS",
      source: "MS",
      id: "MS",
      name: "MS",
      nameZh: "MS 捕鱼",
      // img: path + "/images/game/slot-02.png",
      img: path + "/images/game-vendor/ms.png",
      logo: path + "/images/vendor-logo/ms.png"
    },
  ],
  user: {
    uid: null,
    username: null,
    name: null,
    isLogin: false,
    currentRoute: '',
    vendorsCount: 0,
    wallet: {
      setbank: false,
      totalAmounts: 0,
      sync: '',
      refresh: false,
      deploy: {},
    },
    class: 0,
    login_time: '',
    mobile: '',
    email: '',
    bank: {
      bank_name: '',
      bank_value: '',
      bank_account: '',
      bank_username: '',
    },
    is_cash_password: false,
    created_at: '',
    line: '',
    unread: 0,
    isShowAppDL: true,
    error: false,
    bank_atm: '',
    dzp_chance: 0,

  },
  web: {
    temp: {},
    bulletins: [],
    news: [],
    qt: [],
    ae: [],
    f8: [],
    sa_slot: [],
    //ddfg: [],
    //dg: [],
    ps: [],
    //wm: [],
    ttg: [],
    //og: [],
    rtg: [],
    bng: [],
    ig: [],
    dt: [],
    pg: [],
    ing: [],

    banklists: [],

    tabbarMenus: [{
        iconOn: 'van-icon icon-home',
        iconOff: 'van-icon icon-home-o',
        text: '首页',
        path: '/'
      },
      {
        iconOn: 'van-icon icon-hot',
        iconOff: 'van-icon icon-hot-o',
        text: '优惠',
        path: '/promotion'
      },
      {
        iconOn: 'van-icon icon-register',
        iconOff: 'van-icon icon-register-o',
        text: '注册',
        path: '/register',
      },
      {
        iconOn: 'van-icon icon-service ',
        iconOff: 'van-icon icon-service-o',
        text: '客服',
        // path: '/service',
      },
      {
        iconOn: 'van-icon icon-login',
        iconOff: 'van-icon icon-login-o',
        text: '登录',
        path: '/login'
        //  badge: '5'
      }
    ],


  },
  datalist: [],

}
